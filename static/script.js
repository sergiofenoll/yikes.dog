'use strict';

window.onload = function() {
    for (let el of document.getElementsByClassName("delete")) {
        el.addEventListener("click", closeNotification);
    }
    this.document.getElementById('submit-password-change').addEventListener('click', validatePassword);
};

function closeNotification(ev) {
    let el = ev.target;
    el.parentElement.remove();
}

function validatePassword(ev) {
    ev.preventDefault();
    let new_password = document.getElementById('new_password');
    let confirm_password = document.getElementById('confirm_password');

    if (new_password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords don't match");
    } else {
        confirm_password.setCustomValidity('');
        document.getElementById('form-password-change').submit();
    }
}