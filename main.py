from bcrypt import checkpw, hashpw, gensalt
from flask import Flask, render_template, request, session, flash, redirect, url_for
from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relation, sessionmaker
import bcrypt


Base = declarative_base()

class User(Base):
    __tablename__ = 'virtual_users'
    id = Column(Integer, primary_key=True)
    domain_id = Column(Integer, nullable=False)
    password = Column(String, nullable=False)
    email = Column(String, nullable=False)

    def __repr__(self):
        return f'Email({self.email}, {self.password})'

engine = create_engine('postgresql://username:password@localhost/database')

Session = sessionmaker(bind=engine)
sql_session = Session()

app = Flask(__name__)
app.secret_key = 'my secret key'

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/projects')
def projects():
    return render_template('projects.html')

@app.route('/sign_up')
def sign_up():
    return render_template('sign_up.html')

@app.route('/log_in')
def log_in():
    return render_template('log_in.html')

@app.route('/log_in', methods=['POST'])
def log_in_form():
    if 'email' in session:
        return redirect(url_for('home'))

    email = request.form.get('email')
    password = request.form.get('password')

    user = sql_session.query(User).filter(User.email == email).first()

    if user:
        if checkpw(password.encode('utf8'), user.password.encode('utf8')):
            session['email'] = email
            flash(f'Successfully logged in!', 'is-success')
            return redirect(url_for('home'))
        else:
            flash('The password you filled in was wrong.', 'is-danger')
            return redirect(url_for('home'))
    else:
        flash('The email you filled in is unknown.', 'is-warning')
        return redirect(url_for('home'))

@app.route('/log_out')
def log_out():
    if 'email' not in session:
        return redirect(url_for('home'))
    
    session.pop('email', None)
    flash('Successfully logged out!', 'is-success')
    return redirect(url_for('home'))

@app.route('/user')
def user_page():
    if 'email' not in session:
        flash('You can only access the user page if you are logged in!', 'is-warning')
        return redirect(url_for('home'))
    return render_template('user_page.html')

@app.route('/user', methods=['POST'])
def change_password_form():
    if 'email' not in session:
        flash('You can only change your password if you are logged in!', 'is-warning')
        return redirect(url_for('home'))
    email = session.get('email')
    password = request.form.get('password')
    new_password = request.form.get('new_password')

    user = sql_session.query(User).filter(User.email == email).first()

    if user:
        if checkpw(password.encode('utf8'), user.password.encode('utf8')):
            salt = gensalt()
            hashed = hashpw(new_password.encode('utf8'), salt)
            
            user.password = hashed.decode('utf8')
            sql_session.commit()

            session.pop('email', None)

            flash(f'Successfully changed your password!', 'is-success')
            return redirect(url_for('home'))
        else:
            flash('The password you filled in was wrong.', 'is-danger')
            return redirect(url_for('home'))
    else:
        flash('The email you filled in is unknown.', 'is-warning')
        return redirect(url_for('home'))